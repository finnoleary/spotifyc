CC=gcc
CFLAGS= -g -static -D_XOPEN_SOURCE=700 -D_FORTIFY_SOURCE=2 -std=c99 -Wall -Wextra -pedantic -O0 -fstack-protector-strong -fno-omit-frame-pointer
CPPCHKFLAGS= --std=posix --std=c99 --platform=unix64 --enable=all --suppress=missingIncludeSystem
SPARSEFLAGS= -Wsparse-all -gcc-base-dir `gcc --print-file-name=` 
SCANBUILDFLAGS= -enable-checker core -enable-checker security -enable-checker unix
SCANBUILD= `{ (command -v scan-build-7) && echo 'scan-build-7' || echo 'scan-build' }


MONGOOSE_HEAD=  -I./lib/mongoose
MONGOOSE_SRC=   lib/mongoose/mongoose.c
MONGOOSE_OBJ=   bin/mongoose.o
MONGOOSE_FLAGS= -g -DMG_ENABLE_SSL -DMG_ENABLE_THREADS -DMG_ENABLE_HTTP_WEBSOCKET=0

FROZEN_HEAD=  -I./lib/frozen
FROZEN_SRC=   lib/frozen/frozen.c
FROZEN_OBJ=   bin/frozen.o
FROZEN_FLAGS= 

LDFLAGS+= -ldl
CLIBS= -lssl -lcrypto -ldl -pthread 

LIBRARY_HEAD= $FROZEN_HEAD $MONGOOSE_HEAD -I./libspotifyc/
LIBRARY_SRC= `{ls libspotifyc/*.c}
LIBRARY_OBJ= ${LIBSRC:src/%.c=%.o}

PROGRAM_HEAD= $LIBRARY_HEAD
PROGRAM_FLAGS= $CFLAGS $MONGOOSE_FLAGS $PROGRAM_HEAD
PROGRAM_SRC= src/spotifyc.c
PROGRAM_OBJ= $MONGOOSE_OBJ $FROZEN_OBJ
PROGRAM_LIBS= $CLIBS -L./bin/ -lspotifyc 

do:Q: bin ./bin/spotifyc
  :

bin:V:
  [ ! -e bin ] && mkdir bin || true

./bin/spotifyc: ./bin/libspotifyc.a ./bin/frozen.o $PROGRAM_SRC
  $SCANBD $SCANBDFLAGS $CC $PROGRAM_FLAGS $PROGRAM_SRC $PROGRAM_OBJ $PROGRAM_LIBS -o $target

./bin/libspotifyc.a: ./bin/mongoose.o ./bin/frozen.o $LIBRARY_SRC
  cd libspotifyc && mk
  mv libspotifyc.a ../$target

./bin/mongoose.o: $MONGOOSE_SRC
  $CC -c $MONGOOSE_HEAD $MONGOOSE_FLAGS $MONGOOSE_SRC
  mv *.o bin/

./bin/frozen.o: $FROZEN_SRC
  $CC -c $FROZEN_HEAD $FROZEN_FLAGS $FROZEN_SRC
  mv *.o bin/

check:V:
  sparse $SPARSEFLAGS $LIBMGHEAD $LIBFZHEAD $SRC
  cppcheck $CPPCHECKFLAGS $LIBMGHEAD $LIBFZHEAD --force $SRC

clean:V:
  rm -f bin/*
  cd libspotifyc && mk clean
