#include "mongoose.h"
#include "unistd.h"
#include "string.h"

enum {
	WAITING,
	FINISHED
};

static int status = WAITING;

static void fail_response(struct mg_connection *nc, struct http_message *hm)
{
	const char *data = "There was some problem processing the query string. Debug info:\n";
	mg_printf(nc, "HTTP/1.1 200 OK\r\n"
	              "Content-Type: text/plain\r\n"
		      "\r\n"
		      "%s\r\n"
		      "%s\r\n"
		      "\r\n", 
		      data, hm->message.p);
	nc->flags |= MG_F_SEND_AND_CLOSE;
	status = FINISHED;
}


/* Returns the length of the string or negative if it's bigger than outbuf */
static ssize_t parse_query(const char *qstr, size_t length, int output_fd)
{
	char *s = NULL;
	size_t size = 0;

	if (!qstr || !(s = strstr(qstr, "code="))) {
		return -1;
	}

	s += 5; /* sizeof("code=") */

	while (s[size] && size < length && s[size] != '&' && s[size] != ' ') {
		size++;
	}

	if (s[size] != '&' && s[size] != ' ') { return -1; }

	dprintf(output_fd, "%.*s", size, s);


	return size;
}


static void handle_response(struct mg_connection *nc, struct http_message *hm)
{
	int output_fd = *((int*)(nc->user_data));
	char *response = "Done! You can now close this tab/window!";

	if ((strncmp(hm->method.p, "GET", 3)) != 0) { 
		fail_response(nc, hm);
	}

	if ((parse_query(hm->query_string.p, hm->query_string.len, output_fd)) < 0) {
		fail_response(nc, hm);
	} else {
		status = FINISHED;
	}

	mg_printf(nc, "HTTP/1.1 200 OK\r\n"
	              "Content-Type: text/plain\r\n"
		      "\r\n"
		      "%s\r\n"
		      "\r\n",
		      response);
}


static void server_ev_handler(struct mg_connection *nc, int ev, void *ev_data) {
	struct http_message *hm = (struct http_message *)ev_data;

	if (ev == MG_EV_HTTP_REQUEST) {
		if (mg_vcmp(&hm->uri, "/") == 0) {
			handle_response(nc, hm);
		}
		else {
			mg_printf(nc, "HTTP/1.1 404 Not Found\r\n");
		}
		nc->flags |= MG_F_SEND_AND_CLOSE;
	}
}


static inline int server_valid_port(char *port)
{
	for (; isdigit(*port); port++)
		;

	return (*port == '\0');
}


int do_server(char *port, int output_fd)
{
	struct mg_mgr mgr = { 0 };
	struct mg_connection *nc = { 0 };
	struct mg_bind_opts bind_opts = { 0 };
	const char *err_str = NULL;

	if (!port || !server_valid_port(port)) {
		fprintf(stderr, "Bad or missing port number\n");
		return 1;
	}

	if (output_fd <= 0) {
		fprintf(stderr, "authcode file descriptor not provided\n");
		return 1;
	}

	mg_mgr_init(&mgr, NULL);

	bind_opts.error_string = &err_str;

	if (!(nc = mg_bind_opt(&mgr, port, server_ev_handler, bind_opts))) {
		fprintf(stderr, "Error starting server on port %s: %s\n",
				port, *bind_opts.error_string);
		return 1;
	}

	/* We output the code in parse_query. We pass the file descriptor through
	   nc.user_data which mongoose doesn't touch. Although having seen the
	   mongoose code, I'm not sure I trust that guarantee, ha. */

	nc->user_data = &output_fd;
	mg_set_protocol_http_websocket(nc);
	for (; status == WAITING;) {
		mg_mgr_poll(&mgr, 1000);
	}
	mg_mgr_free(&mgr);

	return 0;
}
