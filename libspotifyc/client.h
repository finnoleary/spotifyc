#include "mongoose.h"
#define MAX_HEADERS MG_MAX_HTTP_HEADERS

#define POLL_TIMEOUT_MS 200

typedef struct {
	int done, error;

	int status;
	char *source;
	char *method, *uri, *protocol, *status_msg, *query;
	struct {
		char *name, *value;
	} headers[MAX_HEADERS];
	char *body;
} Response;

char      *mkquery  (char *s, ...);
char      *mkheader (char *s, ...);
char      *from_query(char *query, char *key, size_t *length);
char      *from_header(Response r, char *name);
char      *_join(char *s, ...);
#define    join(...) _join(__VA_ARGS__, (char*)NULL)

Response   mkrequest(char *method, char *url, char *headers, char *query, char *body);
void       reset_response(Response *r);

Response   get(char *url, char *headers, char *query);
Response   put(char *url, char *headers, char *query, char *body);
Response   post(char *url, char *headers, char *query, char *body);
Response   delete(char *url, char *headers, char *query, char *body);
