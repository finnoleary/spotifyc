#include "client.h"
#include <stdarg.h>

#define offset(src, p)  (size_t)(p - src)

/* defined in mongoose.c but not in mongoose.h, needed for http_connect_v2 */
struct mg_connection *mg_connect_http_base(
    struct mg_mgr *mgr, MG_CB(mg_event_handler_t ev_handler, void *user_data),
    struct mg_connect_opts opts, const char *scheme1, const char *scheme2,
    const char *scheme_ssl1, const char *scheme_ssl2, const char *url,
    struct mg_str *path, struct mg_str *user_info, struct mg_str *host);
 

char *mkquery(char *s, ...)
{
	va_list ap;
	size_t len = 0;
	size_t i = 0;
	int j = 0;
	char *d = s;
	const char *echar = "=&";

	/* Count arguments */

	va_start(ap, s);
	while (s) {
		len += strlen(s) + 1; /* x= | y& | z\0 */
		s = va_arg(ap, char *);
	}
	va_end(ap);
	s = d;

	if (!len || !(d = calloc(len, 1))) { return NULL; }

	va_start(ap, s);
	while (s) {
		len = strlen(s);
		memcpy(d+i, s, len);
		i += len;
		s = va_arg(ap, char*);
		d[i++] = s ? echar[j] : '\0';
		j = (j+1)%2;
	}
	va_end(ap);

	return d;
}


char *mkheader(char *s, ...)
{
	va_list ap;
	size_t len = 0;
	size_t i = 0;
	int j = 0;
	char *d = s;
	const char *echar0 = ":\r";
	const char *echar1 = " \n";

	/* Count arguments */

	va_start(ap, s);
	while (s) {
		len += strlen(s) + 2; /* ": " | "\r\n" */
		s = va_arg(ap, char *);
	}
	va_end(ap);
	s = d;

	if (!(d = calloc(len+1, 1))) { return NULL; }

	va_start(ap, s);
	while (s) {
		len = strlen(s);
		memcpy(d+i, s, len);
		i += len;
		s = va_arg(ap, char*);
		d[i++] = echar0[j];
		d[i++] = echar1[j];
		j = (j+1)%2;
	}
	d[i] = (char)'\0';
	va_end(ap);

	return d;
}


char *from_query(char *query, char *key, size_t *length)
{
	char *s = NULL;
	size_t i = 0;

	if (!query || !key || !length) { return NULL; }

	if (!(s = strstr(query, key))) { return NULL; }
	s += strlen(key) + 1;
	if (s[-1] != '=') { return NULL; }
	
	while (s[i] && s[i] != '&') {
		i++;
	}
	*length = i;

	return s;
}


char *from_header(Response r, char *name)
{
	size_t i = 0;
	if (!name) { return NULL; }
	while (i < MAX_HEADERS) {
		if (!r.headers[i].name) { break; }
		if ((strcmp(name, r.headers[i].name)) == 0) {
			return r.headers[i].value;
		}
		i++;
	}
	return NULL;
}


char *_join(char *s, ...)
{
	va_list ap;
	size_t length = 0;
	char *d = s;

	va_start(ap, s);
	while (s) {
		length += strlen(s);
		s = va_arg(ap, char *);
	}
	s = d;
	va_end(ap);
	
	if (!(d = calloc(length + 1, 1))) { return NULL; }

	va_start(ap, s);
	for (length = 0; s;) {
		memcpy(d+length, s, strlen(s));
		length += strlen(s);
		s = va_arg(ap, char *);
	}
	va_end(ap);

	return d;
}


static void response_handler(struct mg_connection *nc, int ev, void *ev_data)
{
	struct http_message *hm = (struct http_message *)ev_data;
	Response *r = (Response*)nc->user_data;

	if (ev == MG_EV_CONNECT) {
		if ((r->status = *(int *)ev_data)) {
			r->error = 1;
			r->done = 1;
		}
	}
	else if (ev == MG_EV_HTTP_REPLY) {
		size_t i = 0;
		size_t off = 0;
		const char *m = hm->message.p;
		struct mg_str *ms = NULL;

		if (!(r->source = strdup(m))) {
			r->done = 1;
			r->error = errno;
			nc->flags |= MG_F_SEND_AND_CLOSE;
			return;
		}

		r->status = hm->resp_code;
		if ((ms = &(hm->method))->p) {
			off = offset(m, ms->p);
			r->method = r->source + off;
			r->method[ms->len] = (char)0;
		}
		if ((ms = &(hm->uri))->p) {
			off = offset(m, ms->p);
			r->uri = r->source + off;
			r->uri[ms->len] = (char)0;
		}
		if ((ms = &(hm->proto))->p) {
			off = offset(m, ms->p);
			r->protocol = r->source + off;
			r->protocol[ms->len] = (char)0;
		}
		if ((ms = &(hm->resp_status_msg))->p) {
			off = offset(m, ms->p);
			r->status_msg = r->source + off;
			r->status_msg[ms->len] = (char)0;
		}
		if ((ms = &(hm->query_string))->p) {
			off = offset(m, ms->p);
			r->query = r->source + off;
			r->query[ms->len] = (char)0;
		}
		if ((ms = &(hm->body))->p) {
			off = offset(m, ms->p);
			r->body = r->source + off;
			r->body[ms->len] = (char)0;
		}

		for (i = 0; i < MG_MAX_HTTP_HEADERS && hm->header_names[i].p; i++) {
			ms = &(hm->header_names[i]);
			off = offset(m, ms->p);
			r->headers[i].name = r->source + off;
			r->headers[i].name[ms->len] = (char)0;

			ms = &(hm->header_values[i]);
			off = offset(m, ms->p);
			r->headers[i].value = r->source + off;
			r->headers[i].value[ms->len] = (char)0;
		}

		nc->flags |= MG_F_SEND_AND_CLOSE;
		r->done = 1;
	}
	else if (ev == MG_EV_CLOSE) {
		if (!r->done) {
			/* printf("Server closed the connection\n"); */
			r->done = 1;
		}
	}
}


struct mg_connection *http_connect_v2(struct mg_mgr *mgr,
                                      MG_CB(mg_event_handler_t ev_handler, void *user_data),
				      const char *method,
                                      const char *url,
				      const char *query,
                                      const char *extra_headers,
				      const char *body)
{
	struct mg_str user = MG_NULL_STR, null_str = MG_NULL_STR;
	struct mg_str host = MG_NULL_STR, path = MG_NULL_STR;
	struct mbuf auth;
	struct mg_connect_opts opts = {0};
	struct mg_connection *nc = NULL;

	if (!mgr || !method || !url) {
		fprintf(stderr, "Error: Null argument in http_connect_v2\n");
		return NULL;
	}

	nc = mg_connect_http_base(mgr, MG_CB(ev_handler, user_data), opts,
	                          "http", NULL, "https", NULL, url,
	                          &path, &user, &host);

	if (nc == NULL) { return NULL; }

	mbuf_init(&auth, 0);
	if (user.len > 0) { mg_basic_auth_header(user, null_str, &auth); }

	if (body == NULL) body = "";
	if (query == NULL) query = "";
	if (extra_headers == NULL) extra_headers = "";
	if (path.len == 0) path = mg_mk_str("/");
	if (host.len == 0) host = mg_mk_str("");

	mg_printf(nc,
	          "%s %.*s%s%s HTTP/1.0\r\n"
		  "Host: %.*s\r\n"
		  "Content-Length: %" SIZE_T_FMT "\r\n"
		  "%.*s%s\r\n"
		  "%s",
	          method, (int)path.len, path.p,
		  (strlen(query) ? "?" : ""), query,
	          (int) (path.p - host.p), host.p,
		  strlen(body),
		  (int) auth.len, (auth.buf == NULL ? "" : auth.buf),
		  extra_headers,
		  body);

	if (getenv("SPOTIFYC_VERBOSE")) {
		fprintf(stderr,
			"%s %.*s%s%s HTTP/1.0\r\n"
			"Host: %.*s\r\n"
			"Content-Length: %" SIZE_T_FMT "\r\n"
			"%.*s%s\r\n"
			"%s",
			method, (int)path.len, path.p,
			(strlen(query) ? "?" : ""), query,
			(int) (path.p - host.p), host.p,
			strlen(body),
			(int) auth.len, (auth.buf == NULL ? "" : auth.buf),
			extra_headers,
			body);
	}

	mbuf_free(&auth);
	return nc;
}


Response mkrequest(char *method, char *url, char *headers, char *query, char *body)
{
	Response r = {0};
	struct mg_mgr mgr;
	struct mg_connection *nc;

	mg_mgr_init(&mgr, NULL);
	if (!(nc = http_connect_v2(&mgr, response_handler, method, url, query, headers, body))) {
		r.error = errno;
		goto _throw;
	}
	mg_set_protocol_http_websocket(nc);
	nc->user_data = &r;

	while (!r.done) {
		mg_mgr_poll(&mgr, POLL_TIMEOUT_MS);
	}
_throw:
	mg_mgr_free(&mgr);
	return r;
}


void reset_response(Response *r)
{
	if (!r) { return; }
	if (r->source) { free(r->source); r->source = NULL; }
	r->status = r->done = r->error = 0;
}


/* Some boilerplate to make life a smidgen easier */
Response get(char *url, char *headers, char *query)
{
	return mkrequest("GET", url, headers, query, NULL);
}


Response put(char *url, char *headers, char *query, char *body)
{
	
	return mkrequest("PUT", url, headers, query, body);
}


Response post(char *url, char *headers, char *query, char *body)
{
	return mkrequest("POST", url, headers, query, body);
}


Response delete(char *url, char *headers, char *query, char *body)
{
	return mkrequest("DELETE", url, headers, query, body);
}


#ifdef DOTEST


int test_mkheader(void)
{
	char *test_header = "Content-Type: application/x-www-form-urlencoded\r\n"
	                    "x: y\r\n";
	char *header = mkheader("Content-Type", "application/x-www-form-urlencoded",
	                        "x", "y",
				(char)0);
	assert(header && test_header && (strcmp(test_header, header) == 0));
	if (header) { free(header); }
	return 0;
}


int test_mkquery(void)
{
	char *test_query = "code=application/x-www-form-urlencoded&x=y";
	char *query = mkquery("code", "application/x-www-form-urlencoded", "x", "y", (char)0);
	assert(query && test_query && (strcmp(test_query, query) == 0));
	if (query) { free(query); }
	return 0;
}


int main(int argc, char **argv)
{
	test_mkheader();
	test_mkquery();
	return 0;
}


#endif
