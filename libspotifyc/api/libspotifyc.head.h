#define CODESIZE 1024

typedef struct {
	/* private */
	char *client_id, *client_sec;
	char *redirect_uri;
	char *scope;
	int show_dialog;

	char *base64_idsec, *auth_arg, *token_arg;

	char *access_token, *ret_scope, *refresh_token;
	int expires_in;
	struct timespec last_refresh;
} Auth;

int  spc_auth_init(Auth *a, char *client_id, char *client_sec, char *redirect_uri, char *scope, int show_dialog);
int  spc_auth_start(Auth *a, Response *r);
int  spc_auth_complete(char *port, Auth *a, Response *r);
int  spc_auth_refresh(Auth *a, Response *r, int force);
int  spc_auth_load(Auth *a, char *access_token, char *refresh_token, int expires_in, struct timespec last_refresh);
void spc_auth_free(Auth *a);


/* The API naming follows the spotify api naming.
 * Anatomy of a name is: 
 * 	sp_<reference category>_<api function name> 
 *
 * Thus 'sp_follow_user_following_users' is 'user_following_users' in the api category 'follow'
 * The usefulness of knowing where in the documentation the endpoint/function is outweights the
 * potential for these names to be confusing, but I admit it's a very fine line.
 * 
 * There's a lot of amusing redundancy in this API (See: get_users_playlists,
 * get_current_users_playlists), blame spotify for that. I'm sure they have their reasons.
 */

