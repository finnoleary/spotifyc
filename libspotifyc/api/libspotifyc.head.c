#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "mongoose.h"
#include "frozen.h"
#include "client.h"
#include "base64.h"
#include "server.h"

#include "libspotifyc.h"

typedef unsigned char uchar;

static int fill_authheader(Auth *a)
{
	char *plain = NULL;
	if (!a || !a->client_id || !a->client_sec) { return -1; }
	if (!a->base64_idsec) {
		if (!(plain = join(a->client_id, ":", a->client_sec))) { goto _throw; }
		if (!(a->base64_idsec = (char*)base64_encode((uchar*)plain, strlen(plain), NULL))) {
			goto _throw;
		}
		free(plain);
	}
	return 0;

_throw:
	if (plain) { free(plain); }
	return -1;
}


/* I haven't found any documentation on whether mongoose is thread-safe, so
   we'll start the redirect listener in a new process */
static int spawn_server(char *port, char *code, size_t length)
{
	int status = 0;
	pid_t p = -1;
	int fd[2] = { -1, -1 }; /* read, write */
	if (!port || !code) { return -1; }

	/* start server and get response */
	if ((pipe(fd)) < 0)  { perror("pipe"); return 0; }
	if ((p = fork()) < 0) { perror("fork"); goto _throw; }
	else if (p == 0) { /* child */
		close(fd[0]);
		do_server(port, fd[1]);
		exit(0);
	}
	else { /* parent */
		close(fd[1]);
		while ((p = wait(&status)) < 0 && errno == EINTR)
			;
		if (p < 0) {
			printf("wait() error: %s\n%s\n", strerror(errno), code);
			goto _throw;
		}
		if (status != 0) {
			printf("server error: %s\n", code);
			goto _throw;
		}
		if ((read(fd[0], code, length)) < 0) {
			perror("read");
			goto _throw;
		}
	}
	return 0;
_throw:
	if (fd[0] > -1) { close(fd[0]); }
	if (fd[1] > -1) { close(fd[1]); }
	return -1;
}


/* External Interface */


int spc_auth_init(Auth *a, char *client_id, char *client_sec, char *redirect_uri, char *scope, int show_dialog)
{
	Auth ta = (Auth){0};
	if (!a || !client_id || !client_sec || !redirect_uri || !scope) { return -1; }

	ta.client_id     = strdup(client_id);
	ta.client_sec    = strdup(client_sec);
	ta.redirect_uri  = strdup(redirect_uri);
	ta.scope         = strdup(scope);
	ta.show_dialog   = show_dialog;

	*a = ta;

	if ((fill_authheader(a)) < 0) {
		return -1;
	}
	if (!(a->auth_arg = join("Basic ", a->base64_idsec))) {
		return -1;
	}

	return 0;
}


void spc_auth_free(Auth *a)
{
	if (!a) { return; }
	if (a->client_id)     { free(a->client_id);     a->client_id = NULL;     }
	if (a->client_sec)    { free(a->client_sec);    a->client_sec = NULL;    }
	if (a->redirect_uri)  { free(a->redirect_uri);  a->redirect_uri = NULL;  }
	if (a->scope)         { free(a->scope);         a->scope = NULL;         }

	if (a->base64_idsec)  { free(a->base64_idsec);  a->base64_idsec = NULL;  }
	if (a->auth_arg)      { free(a->auth_arg);      a->auth_arg = NULL;      }
	if (a->access_token)  { free(a->access_token);  a->access_token = NULL;  }
	if (a->ret_scope)     { free(a->ret_scope);     a->ret_scope = NULL;     }
	if (a->refresh_token) { free(a->refresh_token); a->refresh_token = NULL; }
	if (a->token_arg)     { free(a->token_arg);     a->token_arg = NULL;     }
}


int spc_auth_start(Auth *a, Response *r)
{
	char *url = "https://accounts.spotify.com/authorize";
	char *q = mkquery("client_id",     a->client_id,
	                  "response_type", "code",
			  "redirect_uri",  a->redirect_uri,
			  "scope",         a->scope,
			  "show_dialog",   (a->show_dialog ? "true" : "false"),
			  (char*)NULL);
	if (!a || !r || !q) { goto _throw; }
	*r = get(url, NULL, q);
	if (r->error || r->status != 303) { goto _throw; }
	free(q);
	return 0;
_throw:
	if (q) { free(q); }
	return -1;
}


int spc_auth_complete(char *port, Auth *a, Response *r)
{
	char *q = NULL;
	char *h = NULL;
	char code[CODESIZE] = { (char)0 };

	if (!port || !a || !r) { goto _throw; }

	/* TODO: At the moment this abstraction is leaky. Fix it up */
	if ((spawn_server(port, code, CODESIZE-1)) < 0) { goto _throw; }
	if (strlen(code) == 0) { goto _throw; }

	/* use answer code to request refresh and access tokens */
	if (!(q = mkquery("grant_type", "authorization_code",
	                  "code", code,
			  "redirect_uri", a->redirect_uri,
			  (char *)NULL))) {
		goto _throw;
	}
	if (!(h = mkheader("Authorization", a->auth_arg,
	                   "Content-Type", "application/x-www-form-urlencoded",
			   "Accept", "*/*",
			   (char*)NULL))) {
		goto _throw;
	}
	*r = post("https://accounts.spotify.com/api/token", h, q, NULL);
	free(q); q = NULL;
	free(h); h = NULL;
	if (r->error || r->status != 200) { goto _throw; }

	if ((json_scanf(r->body, strlen(r->body),
	                "{ access_token: %Q"
			", scope: %Q"
			", expires_in: %d"
			", refresh_token: %Q"
			"}",
			&(a->access_token),
			&(a->ret_scope),
			&(a->expires_in),
			&(a->refresh_token))) < 0) {
		goto _throw;
	}

	if ((clock_gettime(CLOCK_MONOTONIC, &(a->last_refresh))) < 0) {
		goto _throw;
	}
	if (!(a->token_arg = join("Bearer ", a->access_token))) {
		goto _throw;
	}

	return 0;

_throw:
	if (q) { free(q); }
	if (h) { free(h); }
	return -1;
}


int spc_auth_load(Auth *a, char *access_token, char *refresh_token, int expires_in, struct timespec last_refresh)
{
	if (!a || !access_token || !refresh_token || !expires_in) { return -1; }
	if (a->access_token)  { free(a->access_token);  a->access_token = NULL; }
	if (a->refresh_token) { free(a->refresh_token); a->refresh_token= NULL; }

	a->access_token = strdup(access_token);
	a->refresh_token = strdup(refresh_token);
	a->expires_in = expires_in;
	a->last_refresh = last_refresh;

	if (!(a->token_arg = join("Bearer ", a->access_token))) {
		return -1;
	}
	return 0;
}


int spc_auth_refresh(Auth *a, Response *r, int force)
{
	double old, new, delta;
	struct timespec new_time, *old_time;

	char *q = NULL;
	char *h = NULL;
	char *access_token = NULL;
	char *ret_scope = NULL;
	int expires_in = 0;

	if (!a || !a->base64_idsec || !a->refresh_token || !r) { return -1; }
	if (!force) {
		if ((clock_gettime(CLOCK_MONOTONIC, &new_time)) < 0) { return -1; }
		old_time = &(a->last_refresh);

		old = (double)old_time->tv_sec + (double)old_time->tv_nsec / 1000000000.0;
		new = (double)new_time.tv_sec + (double)new_time.tv_nsec / 1000000000.0;
		delta = (double)abs(new - old);

		/*
		printf("old: %f; new: %f; delta: %f; compare: %f; result: %d\n",
		       old, new, delta, ((double)a->expires_in - 60*5),
		       (delta < ((double)a->expires_in - 60*5)));
		*/

		if (delta < (double)a->expires_in) { return 0; }
	}

	if (!(q = mkquery("grant_type", "refresh_token",
	                  "refresh_token", a->refresh_token,
			  (char *)NULL))) {
		goto _throw;
	}
	if (!(h = mkheader("Authorization", a->auth_arg,
	                   "Content-Type", "application/x-www-form-urlencoded",
			   "Accept", "*/*",
			   (char*)NULL))) {
		goto _throw;
	}
	*r = post("https://accounts.spotify.com/api/token", h, q, NULL);
	if (r->error || r->status != 200) { goto _throw; }

	if ((json_scanf(r->body, strlen(r->body),
	                "{ access_token: %Q"
			", scope: %Q"
			", expires_in: %d"
			"}",
			&access_token,
			&ret_scope,
			&expires_in)) < 0) {
		goto _throw;
	}

	if (a->token_arg)    { free(a->token_arg);    a->token_arg = NULL;    }
	if (a->access_token) { free(a->access_token); a->access_token = NULL; }
	if (a->ret_scope)    { free(a->ret_scope);    a->ret_scope = NULL;    }

	if (!(a->token_arg = join("Bearer ", access_token))) {
		goto _throw;
	}
	if ((clock_gettime(CLOCK_MONOTONIC, &(a->last_refresh))) < 0) {
		goto _throw;
	}

	a->access_token = access_token;
	a->ret_scope    = ret_scope;
	a->expires_in   = expires_in;

	return 1;
_throw:
	if (!q)             { free(q); }
	if (!h)             { free(h); }
	if (!access_token)  { free(access_token); }
	if (!ret_scope)     { free(ret_scope); }
	return -1;
}
