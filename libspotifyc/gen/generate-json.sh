#!/bin/bash

file="spotify-api.json"

[[ -e $file ]] && echo "Please remove the $file" && exit 1

echo "[" >> $file
last_line=$(cat url-list | tail -n 1)
for line in $(cat url-list); do
	echo "Processing url $line"
	./scrape-url.sh $line >> $file
	[[ "$line" != "$last_line" ]] && printf ",\n" >> $file || printf "\n" >> $file
done
echo "]" >> $file
