#!/usr/bin/env python3
import json
import re


# => "spc_category_name_..."
def process_name(category, name):
	return "spc_" + re.sub("-", "_", category) + "_" + re.sub("-", "_", name)


# ["...?x=", ...] => ["...", ...]
def sanitize_url(url):
	if type(url) is list:
		url0 = url[0].split("?")[0]
		url[0] = url0
	return url


# ["a/", "$b"] => "b"
# ["a/", "$b", "/c"] => "b"
def parameter_name(url):
	if type(url) is list:
		# u is ["$name"] so we process to "name"
		return [u for u in url if u[0:1] == '$'][0][1:]
	else:
		return ""


# => ["Auth *a", ...]
def build_parameters(urlparam, query, body):
	parameters = ["Auth *a", "Response *r"]
	if urlparam != "":
		parameters.append("char *" + urlparam)
		
	if query != "no":
		parameters.append("char *query")
	
	if body != "no":
		parameters.append("char *body")
	
	return parameters


# => "int spc_foo_bar(Auth *a, ...)"
def write_headers(h, name, parameters):
	c_parameters = str.join(", ", parameters)
	string = "int " + name + "(" + c_parameters + ")"
	print(string, file=h)


# ["a/", "b"] => [["baseurl", "a/"]]]
# ["a/", "b", "/c"] => [["baseurl", "a/"], ["endurl", "/c"]]
def build_variables(h, url):
	varlist = []
	if len(url) == 2:
		varlist.append(["baseurl", url[0]])
	elif len(url) == 3:
		varlist.append(["baseurl", url[0]])
		varlist.append(["endurl", url[2]])
	return varlist


# => "char baseurl = \"...\"\nchar ..."
def write_variables(h, variable_list, url):
	s = len("char *")
	print("\tchar *h = NULL;", file=h)
	if len(variable_list) > 0:
		print("\tchar *url = NULL;", file=h)
		for name in variable_list:
			print("\tchar " + name[0] + "[] = \"" + name[1] + "\";", file=h)
	else:
		print("\tchar url[] = \"" + url + "\";", file=h)


# => "if (!a || !r || !param || !quey || !body) { return -1; }
def write_guard(h, parameter_name, query, body):
	list = ["a", "r"]
	if parameter_name != "":
		list.append(parameter_name)

	if query == "required":
		list.append("query")

	if body == "required":
		list.append("body")
	
	s = str.join(" || ",  ["!" + s for s in list])
	print("\tif (" + s + ") { return -1; }", file=h)


# => "if (!(h = mkheader(...))) { return -1; }"
def write_mkheader(h, content_type):
	if content_type == "yes":
		print("\tif (!(h = mkheader(\"Authorization\", a->token_arg,", file=h)
		print("\t                   \"Content-Type\", \"application/json\",", file=h)
		print("\t                   (char*)NULL))) {", file=h);
	else:
		print("\tif (!(h = mkheader(\"Authorization\", a->token_arg, (char*)NULL))) {", file=h)

	print("\t\treturn -1;", file=h)
	print("\t}", file=h)


# => "if (!(url = join(...))) { goto _throw; }"
def write_join(h, urlparam, url):
	if type(url) is list:
		vars = ["baseurl", urlparam]
		if len(url) == 3:
			vars.append("endurl")
		s = str.join(", ", vars)
		print("\tif (!(url = join(" + s + "))) {", file=h)
		print("\t\tgoto _throw;", file=h)
		print("\t}", file=h)


# => "*r = type(url, h, ...);
def write_request(h, type, query, body):
	vars = ["url", "h"]

	vars.append("query" if query != "no" else "NULL")
	if type != "get":
		vars.append("body" if body != "no" else "NULL")

	s = str.join(", ", vars)
	print("\t*r = " + type + "(" + s + ");", file=h)


# => "free(h); free(url); _throw: ..."
def write_frees_and_return(h, url):
	has_allocated_url = type(url) is list
	print("\tfree(h);", file=h)
	if has_allocated_url:
		print("\tfree(url);", file=h)
	print("\treturn 0;", file=h)
	if has_allocated_url:
		print("_throw:", file=h)
		print("\tfree(h);", file=h)
		print("\tfree(url);", file=h)
		print("\treturn -1;", file=h)


# Tie everything together
def write_function(h, entry):
	url = sanitize_url(entry["url"])
	query = entry["query"]
	body = entry["body"]

	function_name = process_name(entry["category"], entry["name"])
	urlparam = parameter_name(url)
	parameter_list = build_parameters(urlparam, query, body)
	variable_list = build_variables(h, url)

	write_headers(h, function_name, parameter_list)
	print("{", file=h)

	write_variables(h, variable_list, url)
	print("", file=h)

	write_guard(h, urlparam, query, body)
	write_mkheader(h, entry["content_type"])
	write_join(h, urlparam, url)
	print("", file=h)

	write_request(h, entry["type"], query, body)
	write_frees_and_return(h, url)

	print("}", file=h)


# => "int spc_category_name(Auth *a, ...);"
def write_declaration(h, entry):
	name = process_name(entry["category"], entry["name"])
	urlparam = parameter_name(entry["url"])
	parameters = build_parameters(urlparam, entry["query"], entry["body"])
	c_parameters = str.join(", ", parameters)
	string = "int " + name + "(" + c_parameters + ");"
	print(string, file=h)


def write_interface_struct(h, entry):
	name = process_name(entry["category"], entry["name"])
	urlparam = parameter_name(entry["url"])
	parameters = build_parameters(urlparam, entry["query"], entry["body"])
	query = entry["query"]
	body = entry["body"]

	if query == "optional":
		qstr = "CANQUERY"
	elif query == "required":
		qstr = "MUSTQUERY"
	else:
		qstr="NOQUERY"

	if body == "optional":
		bstr = "CANBODY"
	elif body == "required":
		bstr = "MUSTBODY"
	else:
		bstr="NOBODY"
	
	nargs = len(parameters) - 2
	callback="CB" + str(nargs) + "(" + name + ")"

	varlist = [ "\"" + entry["category"] + "\""
	          , "\"" + entry["name"] + "\"" if entry["category"] != entry["name"] else "NULL"
	          , "MUSTURL" if urlparam != "" else "NOURL"
		  , qstr
		  , bstr
		  , callback
                  , "\"" + entry["doc"] + "\""
	          ]
	print("\t{ " + str.join(", ", varlist) + " },", file=h)


# main junk
with open("spotify-api.json", "r") as handle:
	api = json.load(handle)

with open("../api/libspotifyapi.h", "w") as handle:
	url_base = "https://developer.spotify.com/documentation/web-api/reference/"
	last_category=""
	for fun in api:
		if fun["category"] != last_category:
			last_category = fun["category"]
			print("\n/* " + url_base + last_category + " */", file=handle)
		write_declaration(handle, fun)

with open("../api/libspotifyapi.c", "w") as handle:
	for fun in api:
		write_function(handle, fun)
		print("\n", file=handle)

with open("../../src/spotifyc.interface.h", "w") as handle:
	for fun in api:
		write_interface_struct(handle, fun)
