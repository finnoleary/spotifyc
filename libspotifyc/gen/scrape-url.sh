#!/bin/bash
# for line in $(cat url-list); do
# done

# URLS FOR TESTING + features that they test

# transfer-a-users-playback has: PUT, url a/, body required (with 'body parameters')
# line="https://developer.spotify.com/documentation/web-api/reference/player/transfer-a-users-playback"

# remove-tracks-playlist has: DELETE, url a/{b}/c, content-type, body required (with 'request data')
# line="https://developer.spotify.com/documentation/web-api/reference/playlists/remove-tracks-playlist"

# start-a-users-playback has: PUT, url a/, query optional, body optional
# line="https://developer.spotify.com/documentation/web-api/reference/player/start-a-users-playback"

# get-users-profile has: GET, url a/{b}, query no, body no
# line="https://developer.spotify.com/documentation/web-api/reference/users-profile/get-users-profile"

# set-volume-for-users-playback has: PUT, url a/, query required, body no
# line="https://developer.spotify.com/documentation/web-api/reference/player/set-volume-for-users-playback"

if [[ "$1" == "--test" ]]; then
	test="true"
	url="$2"
else
	test="false"
	url="$1"
fi


# Step one: Take the last two elements of the URL which are the api category and the api name
categories=$(printf "%s\n" "$url" | tr '/' '\n' | tail -n 2)

category=$(printf "%s\n" "$categories" | head -n 1)
function=$(printf "%s\n" "$categories" | tail -n 1)

# Step one OUTPUTS:
# echo $category
# echo $function


# Step two: Grab the webpage, pull out command and parse the endpoint into a python array
data=$(lynx $url -dump | sed '1,/^Endpoint/d' - | tail -n +2)

command_and_endpoint=$(printf "%s\n" "$data" | head -n 1)
command=$(printf "%s\n" $command_and_endpoint | sed 's/\([A-Z]*\).*/\1/' - | tr 'A-Z' 'a-z')
endpoint=$(printf "%s\n" $command_and_endpoint | tr ' ' '\n' | tail -n 1)
has_content_type=$( printf "%s" "$data\n" | sed '/Response Format/,$d' - | grep "Content-Type" - )
content_type=$( [[ $has_content_type ]] && echo "yes" || echo "no" )

# no time like the present to build the url string / array

cond=$(printf "%s\n" "$endpoint" | grep -qi "{" - && echo $?)
if [[ ! -z $cond ]]; then
	_endpoint=$(printf "%s\n" "$endpoint" | tr '{' '\n' | tr '}' '\n')
	url_base=$(printf "%s\n" $_endpoint | head -n 1)
	url_var=$(printf "%s\n" $_endpoint | tail -n 2 | head -n 1)
	url_end=$(printf "%s\n" $_endpoint | tail -n 1)

	if [[ "$url_base" == "$url_var" ]]; then
		url_resultend=", \"\$$url_end\"]"
	else
		url_resultend=", \"\$$url_var\", \"$url_end\"]"
	fi

	url_value="[\"$url_base\"$url_resultend"
else
	url_value="\"$endpoint\""
fi

# Step two OUTPUTS:
# echo $command
# echo $url_value
# echo $content_type


# Step three: Test for "Query Format"
# if it exists grab the text between Query Parameters and Response Format/Body Parameters
cond=$(printf "%s\n" "$data" | grep "^Query Parameters" && echo $?)

if [[ "$cond" ]]; then
	# Remove from "Body Content" or "Response Format" to the end
	query_format_info=$(printf "%s\n" "$data" | sed -e '1,/Query Parameters/d' \
	                                                -e '/Response Format/,$d'  \
							-e '/Body Parameters/,$d'  - )
	query_required=$(printf "%s\n" $query_format_info | grep "Required" - && echo $?)
	query=$( [[ $query_required ]] && echo "required" || echo "optional" )
else
	query="no"
fi

# Step three OUTPUTS:
# echo $query

# Step four: Test for "Body Parameters"
# if it exists we grab the text between that and Response Format
cond=$(printf "%s\n" "$data" | grep -Eo "^Body Parameters|Request data" - | head -n 1)

if [[ "$cond" ]]; then
	body_format_info=$(printf "%s\n" "$data" | sed -e "1,/$cond/d" \
	                                               -e '/Response Format/,$d' -)
	body_required=$(printf "%s\n" $body_format_info | grep "Required." - && echo $?)
	body=$( [[ $body_required ]] && echo "required" || echo "optional" )
else
	body="no"
fi

# Step four OUTPUTS:
# echo $body

# Step five: output the crap

if [[ "$test" == "true" ]]; then
	echo "$category $function $command $content_type $url_value $query $body"
else
	echo " { \"category\":     \"$category\"     "
	echo " , \"name\":         \"$function\"     "
	echo " , \"type\":         \"$command\"      "
	echo " , \"content_type\": \"$content_type\" "
	echo " , \"url\":          $url_value        "
	echo " , \"doc\":          \"$category/$function\""
	echo " , \"query\":        \"$query\"        "
	echo " , \"body\":         \"$body\"         "
	printf "%s" " }"
fi
