#!/bin/bash

urls=(https://developer.spotify.com/documentation/web-api/reference/player/transfer-a-users-playback
      https://developer.spotify.com/documentation/web-api/reference/playlists/remove-tracks-playlist
      https://developer.spotify.com/documentation/web-api/reference/player/start-a-users-playback
      https://developer.spotify.com/documentation/web-api/reference/users-profile/get-users-profile
      https://developer.spotify.com/documentation/web-api/reference/player/set-volume-for-users-playback     )


# This is absolutely lazy, yes. This is fragile, yes. Put it on the TODO
tests=("player transfer-a-users-playback put no \"https://api.spotify.com/v1/me/player\" no required"
       "playlists remove-tracks-playlist delete yes [\"https://api.spotify.com/v1/playlists/\", \"\$playlist_id\", \"/tracks\"] no required"
       "player start-a-users-playback put no \"https://api.spotify.com/v1/me/player/play\" optional optional"
       "users-profile get-users-profile get no [\"https://api.spotify.com/v1/users/\", \"\$user_id\"] no no"
       "player set-volume-for-users-playback put no \"https://api.spotify.com/v1/me/player/volume\" required no"
      )

length=$(seq 0 $(( ${#urls[@]} - 1 )) )
for i in $length; do
	url="${urls[i]}"
	test="${tests[i]}"
	output=$(./scrape-url.sh --test $url)
	if [[ "$output" != "$test" ]]; then
		echo "Test failed on $url"
		echo "$output"
		echo "$test"
	fi
done

echo "Tests pass"
