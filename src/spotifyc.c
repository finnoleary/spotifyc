#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "mongoose.h"
#include "frozen.h"
#include "client.h"
#include "base64.h"

#include "libspotifyc.h"

enum {
	NOURL = 0,
	MUSTURL,

	NOQUERY = 0,
	CANQUERY,
	MUSTQUERY,

	NOBODY = 0,
	CANBODY,
	MUSTBODY,
};

#define wprint(...) if (settings.verbose) { fprintf(stderr, __VA_ARGS__); }

#define OPTIONCMP(value, short, long) \
	(   ((short) && (strcmp((value), (short))) == 0) \
	 || ((long)  && (strcmp((value), (long))) == 0)  )

#define CB0(name)  0, .cb.zero  = name
#define CB1(name)  1, .cb.one   = name
#define CB2(name)  2, .cb.two   = name
#define CB3(name)  3, .cb.three = name

struct {
	int verbose;
} settings = {
	0
};

struct iface {
	char *category_name;
	char *function;
	int url;
	int query;
	int body;

	int type;
	union {
		int (*three)(Auth *, Response *, char *, char *, char *);
		int (*two)  (Auth *, Response *, char *, char *);
		int (*one)  (Auth *, Response *, char *);
		int (*zero) (Auth *, Response *);
	} cb;
	char *docurl;
} interface[] = {
#include "spotifyc.interface.h"
	{ NULL }
};

static const char docurl[] = "https://developer.spotify.com/documentation/web-api/reference/";


int has_authvars(void)
{
	return getenv("SPOTIFY_CLIENT_ID")
	    && getenv("SPOTIFY_CLIENT_SECRET")
	    && getenv("SPOTIFY_REDIRECT_URI")
	    && getenv("SPOTIFY_REDIRECT_PORT");
}


int has_lastauthvars(void)
{
	return getenv("SPOTIFY_ACCESS_TOKEN")
	    && getenv("SPOTIFY_REFRESH_TOKEN")
	    && getenv("SPOTIFY_REFRESH_TIME")
	    && getenv("SPOTIFY_EXPIRY_TIME");
}


void whine_noauthvars(void)
{
	if (!getenv("SPOTIFY_CLIENT_ID")) {
		fprintf(stderr, "Missing SPOTIFY_CLIENT_ID environment variable\n");
	}
	if (!getenv("SPOTIFY_CLIENT_SECRET")) {
		fprintf(stderr, "Missing SPOTIFY_CLIENT_SECRET environment variable\n");
	}
	if (!getenv("SPOTIFY_REDIRECT_URI")) {
		fprintf(stderr, "Missing SPOTIFY_REDIRECT_URI environment variable\n");
	}
	if (!getenv("SPOTIFY_REDIRECT_PORT")) {
		fprintf(stderr, "Missing SPOTIFY_REDIRECT_PORT environment variable\n");
	}
}       

void whine_nolastauthvars(void)
{
	fprintf(stderr, "No environment variables found from authentication.\n"
	       "Please run 'spotifyc auth'\n");
}


void whine_help(void)
{
	const char *s = "spotifyc auth\n"
	                "spotifyc reauth [-f|--force]\n"
			"spotifyc list\n"
			"spotifyc info [-v|--verbose] <category> <function>\n"
	                "spotifyc [-v|--verbose] <category> <function>"
			  " [-u <path parameter>] [-q <query>] [-b <body>]\n";
	fprintf(stderr, "%s", s);
}


void whine(char *s, ...)
{
	va_list ap;
	va_start(ap, s);
	vfprintf(stderr, s, ap);
	va_end(ap);
}


void write_response(Response *r)
{
	int i = 0;
	fprintf(stderr,
	       "{ \"status\": %u\n"
	       ", \"uri\": \"%s\"\n"
	       ", \"protocol\": \"%s\"\n"
	       ", \"status_msg\": \"%s\"\n"
	       ", \"query\": \"%s\"\n"
	       ", \"headers\":",
	       r->status,
	       r->uri,
	       r->protocol,
	       r->status_msg,
	       r->query);

	for (i = 0; i < MAX_HEADERS && r->headers[i].name; i++) {
		if (i == 0) { fprintf(stderr, "\n\t{ "); }
		else { fprintf(stderr, "\n\t, "); }

		fprintf(stderr,
		       "\"%s\": \"%s\"",
		       r->headers[i].name,
		       r->headers[i].value);
	}
	fprintf(stderr,
	       "%s}\n"
	       ", \"body\": %s\n"
	       "}\n",
	       (r->headers[0].name) ? " " : "{",
	       r->body ? r->body : "{}");
}


int fresh_authenticate(Auth *a)
{
	char *port = getenv("SPOTIFY_REDIRECT_PORT");
	Response r = {0};

	if ((spc_auth_start(a, &r)) < 0) {
		write_response(&r);
		return -1;
	}

	fprintf(stderr,
	       "Please access '%s' to continue authentication\n",
	       from_header(r, "Location"));

	reset_response(&r);

	if ((spc_auth_complete(port, a, &r)) < 0) {
		write_response(&r);
		return -1;
	}

	fprintf(stderr,
	       "Authentication complete\n"
	       "You will want to set the following variables in your environment:\n"
	       "export SPOTIFY_ACCESS_TOKEN='%s'\n"
	       "export SPOTIFY_REFRESH_TOKEN='%s'\n"
	       "export SPOTIFY_EXPIRY_TIME='%d'\n"
	       "export SPOTIFY_REFRESH_TIME='%ld:%ld'\n",
	       a->access_token,
	       a->refresh_token,
	       a->expires_in,
	       a->last_refresh.tv_sec, a->last_refresh.tv_nsec);

	return 0;
}


int load_lastauthvars(Auth *a)
{
	char *access_token, *refresh_token;
	int expires_in = -1;
	struct timespec last_refresh = {0};

	if ((sscanf(getenv("SPOTIFY_REFRESH_TIME"), "%ld:%ld",
	            &(last_refresh.tv_sec), &(last_refresh.tv_nsec))) < 0) {
		perror("sscanf");	
		goto _throw;
	}
	if ((sscanf(getenv("SPOTIFY_EXPIRY_TIME"), "%d", &expires_in)) < 0) {
		perror("sscanf");	
		goto _throw;
	}
	if (!(access_token = getenv("SPOTIFY_ACCESS_TOKEN"))) {
		fprintf(stderr, "SPOTIFY_ACCESS_TOKEN suddenly became NULL\n");
		goto _throw;
	}
	if (!(refresh_token = getenv("SPOTIFY_REFRESH_TOKEN"))) {
		fprintf(stderr, "SPOTIFY_REFRESH_TOKEN suddenly became NULL\n");
		goto _throw;
	}
	
	if ((spc_auth_load(a, access_token, refresh_token,
	                      expires_in, last_refresh)) < 0) {
		goto _throw;
	}

	return 0;
_throw:
	if (access_token)  { free(access_token);  access_token = NULL;  }
	if (refresh_token) { free(refresh_token); refresh_token = NULL; }
	return -1;
	
}


int reauthenticate(Auth *a, int force)
{
	int status = 0;
	Response r = {0};
	if ((status = spc_auth_refresh(a, &r, force)) < 0) {
		write_response(&r);
		return -1;
	}

	if (status == 1) {
		fprintf(stderr,
		       "Reauthentication complete\n"
		       "You will want to set the following variables in your environment:\n"
		       "export SPOTIFY_ACCESS_TOKEN='%s'\n"
		       "export SPOTIFY_REFRESH_TOKEN='%s'\n"
		       "export SPOTIFY_REFRESH_TIME='%d'\n"
		       "export SPOTIFY_EXPIRY_TIME='%ld:%ld'\n",
		       a->access_token,
		       a->refresh_token,
		       a->expires_in,
		       a->last_refresh.tv_sec, a->last_refresh.tv_nsec);
	}
	else if (status == 0) {
		fprintf(stderr, "Not close enough to the expiry deadline to bother reauthing\n");
	}
	else {
		if (errno) { perror(""); }
		fprintf(stderr, "Fatal error while attempting to reauthenticate\n");
	}
	return 0;
}


int newauth(Auth *a)
{
	char *client_id = getenv("SPOTIFY_CLIENT_ID");
	char *client_sec = getenv("SPOTIFY_CLIENT_SECRET");
	char *redirect_uri = getenv("SPOTIFY_REDIRECT_URI");
	char *scopes = join("user-read-recently-played",   "%20",
	                    "user-read-currently-playing", "%20",
	                    "user-read-playback-state",    "%20",
	                    "user-modify-playback-state",  "%20",
	                    "user-read-email",             "%20",
	                    "user-read-birthdate",         "%20",
	                    "user-read-private",           "%20",
	                    "app-remote-control",          "%20",
	                    "streaming",                   "%20",
	                    "playlist-read-private",       "%20",
	                    "playlist-modify-public",      "%20",
	                    "playlist-modify-private",     "%20",
	                    "playlist-read-collaborative", "%20",
	                    "user-follow-read",            "%20",
	                    "user-top-read",               "%20",
	                    "user-library-modify",         "%20",
	                    "user-library-read",           "%20",
	                    "user-follow-modify",          "%20");
	if ((spc_auth_init(a, client_id, client_sec, redirect_uri, scopes, 0)) < 0) {
		if (errno) { perror("calloc"); }
		goto _throw;
	}
	return 0;

_throw:
	spc_auth_free(a);
	return -1;
}


int lookup_interface(char **argv, int i, int *index)
{
	size_t j = 0;

	if (!argv[i]) {
		fprintf(stderr, "Function or category name missing\n");
		return -1;
	}

	for (j = 0; interface[j].category_name; j++) {
		if ((strcmp(argv[i], interface[j].category_name)) != 0) {
			continue;
		}
		/* If the function we want to invoke doesn't take a name argument */
		if (!interface[j].function && (!argv[i+1] || argv[i+1][0] == '-')) {
			break;
		}
		/* If the function we want to invoke has a name */
		else if (interface[j].function && argv[i+1] && (strcmp(argv[i+1], interface[j].function)) == 0) {
			break;
		}
	}

	if (!interface[j].category_name) {
		fprintf(stderr, "Function or category name missing\n");
		return -1;
	}

	if (index) { *index = j; }
	return i + (interface[j].function != NULL);
}


int exec_one(size_t index, Auth *a, Response *r, char *url, char *query, char *body)
{
	char *arg = (interface[index].url) ? url
	         : (interface[index].query) ? query
	         : body;

	return interface[index].cb.one(a, r, arg);
}


int exec_two(size_t index, Auth *a, Response *r, char *url, char *query, char *body)
{
	char *arg1 = (interface[index].url)   ? url : query;
	char *arg2 = (!interface[index].body) ? query : body;

	return interface[index].cb.two(a, r, arg1, arg2);
}


int do_request(int index, Auth *a, Response *r, char *url, char *query, char *body)
{
	if (!interface[index].category_name) { fprintf(stderr, "Called function doesn't exist??\n"); }

	wprint("Interface index possible argument number: %d\n", interface[index].type);
	wprint("Setting SPOTIFYC_VERBOSE\n");
	if (settings.verbose && (setenv("SPOTIFYC_VERBOSE", "1", 1)) < 0) {
		if (errno) { perror("setenv"); }
	}

	switch (interface[index].type) {
	case 0: return interface[index].cb.zero(a, r);
	case 1: return exec_one(index, a, r, url, query, body);
	case 2: return exec_two(index, a, r, url, query, body);
	case 3: return interface[index].cb.three(a, r, url, query, body);
	default: fprintf(stderr, "Unrecognized number of arguments for %s %s\n",
	                 interface[index].category_name,
	                 interface[index].function ? interface[index].function : "");
	}
	return -1;
}


int parse_query(char **argv, char **query)
{
	char *result = NULL;
	size_t length = 0;
	size_t i = 0;
	size_t n = 0;

	/* Figure out how many arguments to convert into query string */
	for (; argv[n]; n++) {
		if (OPTIONCMP(argv[n], "-u", "--url") || OPTIONCMP(argv[n], "-q", "--query")
		 || OPTIONCMP(argv[n], "-b", "--body")) {
			break;
		}
	}

	/* Get the length of the key=value strings */
	for (; argv[i] && i < n; i++) {
		length += strlen(argv[i]);
	}
	/* Figure out how many joining characters to add. This is
	   essentially one for each argument if you include the leading qmark */
	length += n;

	if (!(result = calloc(length+1, 1))) {
		perror("calloc");
		return -1;
	}

	for (length = i = 0; i < n; i++) {
		size_t l = strlen(argv[i]);
		if (i > 0) { result[length] = '&'; length++; }
		memcpy(result+length, argv[i], l);
		length += l;
	}

	*query = result;

	return n;
}


void make_request(int index, Auth *a, char *url, char *body, char *query)
{
	Response r = {0};
	do_request(index, a, &r, url, query, body);
	write_response(&r);
	reset_response(&r);
}


int parse_options(char **argv, Auth *a)
{
	int i = 0;
	int index = -1;
	char *url = NULL;
	char *body = NULL;
	char *query = NULL;

	if (!argv[1] || OPTIONCMP(argv[1], "-h", "--help")) {
		whine_help();
		return -1;
	}
	else if (OPTIONCMP(argv[1], "-l", "list")) {
		size_t i = 0;
		for (i = 0; interface[i].category_name; i++) {
			printf("%s %s\n",
			       interface[i].category_name,
			       interface[i].function ? interface[i].function : "");
		}
		return -1;
	}
	else if (OPTIONCMP(argv[1], "-i", "info")) {
		struct iface *I = NULL;
		if ((i = lookup_interface(argv, 2, &index)) < 0) {
			return -1;
		}
		I = interface+index;
		printf("spotifyc %s%s%s %s%s%s%s%s\n%s%s\n",
		       I->category_name,
		       I->function ? " " : "",
		       I->function ? I->function : "",
		       I->url == MUSTURL ? "-u <path parameter> " : "",
		       I->query == CANQUERY ? "[-q 'key=value' ...] " : "",
		       I->query == MUSTQUERY ? "-q 'key=value' ... " : "",
		       I->body == CANBODY ? "[-b <body>] " : "",
		       I->body == MUSTBODY ? "-b <body> " : "",
		       docurl, I->docurl);
		return -1;
	}

	if (!has_authvars()) { whine_noauthvars(); return -1; }

	if ((newauth(a)) < 0) { perror("newauth"); return -1; }

	if (OPTIONCMP(argv[1], "auth", NULL)) {
		return fresh_authenticate(a);
	}

	if (!has_lastauthvars()) { whine_nolastauthvars(); return -1; }

	if ((load_lastauthvars(a)) < 0) { return -1; }

	if (OPTIONCMP(argv[1], "reauth", NULL)) {
		int force = (argv[2] && OPTIONCMP(argv[2], "-f", "--force"));
		return reauthenticate(a, force);
	}

	if ((i = lookup_interface(argv, 1, &index)) < 0) {
		return -1;
	}

	for (; argv[i]; i++) {
		if ((OPTIONCMP(argv[i], "-v", "--verbose"))) {
			settings.verbose = 1;
		}
		if ((OPTIONCMP(argv[i], "-u", "--url"))) {
			if (interface[index].url == NOURL) {
				whine("Got URL argument when not supported by command\n");
				return -1;
			}
			if (!argv[i+1]) {
				whine("Missing argument for %s\n", argv[i]);
				return -1;
			}
			i++;
			url = argv[i];
			wprint("Found url argument: '%s'\n", url);
		}
		else if ((OPTIONCMP(argv[i], "-b", "--body"))) {
			if (interface[index].body == NOBODY) {
				whine("Got Body argument when not supported by command\n");
				return -1;
			}
			if (!argv[i+1]) {
				whine("Missing argument for %s\n", argv[i]);
			}
			i++;
			body = argv[i];
			wprint("Found body argument: '%s'\n", url);
		}
		else if ((OPTIONCMP(argv[i], "-q", "--query"))) {
			if (interface[index].query == NOQUERY) {
				whine("Got Query argument when not supported by command\n");
				return -1;
			}
			if (!argv[i+1]) {
				whine("Missing argument for %s\n", argv[i]);
			}

			/* Because you just _know_ someone's going to specify
			   url twice at some point. And programs crashing and
			   burning is Bad, ok? */
			if (query) { free(query); query = NULL;}

			if ((i += parse_query(argv+i+1, &query)) < 0) {
				whine("Fatal error while building query string\n");
			}
			wprint("Found query argument: '%s'\n", url);
		}
	}

	make_request(index, a, url, body, query);
	if (query) { free(query); }

	return 0;
}


int main(int argc, char **argv)
{
	Auth a = {0};
	if ((parse_options(argv, &a)) < 0) {
		spc_auth_free(&a);
		return 1;
	}
	spc_auth_free(&a);
	return 0;
}
